discorddelete is a python 3.4 package that installs a command line tool (discdel) to delete your Discord (http://discordapp.com) messages.

Discord keeps every message, forever. To infinity. I don't like that at all so I wrote this.

It could be nicer but it works as is. I still have some TODO items to cross off but it probably won't get too much nicer.

to install use pip + git. something like
`pip3 install --user --upgrade git+https://gitlab.com/gityob/discorddelete.git#egg=discorddelete` or `python3 -m pip etc`

After it's installed properly the `discdel` command should be available. Just run it. Or `discdel --help` if you want.