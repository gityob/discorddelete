import os
import sys

import click
from click import echo, secho
import keyring
from discord.errors import LoginFailure

from . import config
from .delete import DeleterClient


appname = os.path.basename(os.path.dirname(__file__))
keyring_service = "discord" # creds will be stored in keyring under this name

CONTEXT_SETTINGS = {
    "default_map": {
        "usern": config.data["last_username"] or None,
        "pw": lambda: keyring.get_password(
            keyring_service, config.data["last_username"]) or None
    }
}

@click.command(context_settings=CONTEXT_SETTINGS)
@click.option("--usern", required=True,
              prompt="Discord username/email",
              default=config.data["last_username"] or None,
              help="Discord username/email.")
@click.option("--pw", type=click.STRING, required=True,
              default=lambda: keyring.get_password(
                  keyring_service, config.data["last_username"]) or None,
              prompt="Discord password", hide_input=True,
              help="Discord password.")
@click.option("--age-seconds",
              type=int,
              help="Age of message (in seconds) before it should be deleted.")
@click.option("--age-minutes",
              type=int,
              help="Age of message (in minutes) before it should be deleted.")
@click.option("--age-hours",
              type=int,
              help="Age of message (in hours) before it should be deleted.")
@click.option("--log-limit",
              default=lambda: int(config.data.get("log_limit")), type=int,
              help="How many history/log message to fetch at once.")
@click.option("--del-hist/--no-del-hist",
              default=lambda: config.config.getboolean(appname, "del_hist"),
              help=("Search old messages or only watch for new ones?"
                    " --del-hist is default unless you changed the config"))
@click.option("--saveprompt/--no-saveprompt", default=True,
              help=("Prompt to save settings or not."
                    " --saveprompt is default "
                    "and it doesn't show unnecessarily."))
def cli(usern, pw, age_seconds, age_minutes, age_hours, log_limit, del_hist,
        saveprompt):
    """
    Delete all of your Discord (http://discordapp.com) messages.
    \n
    Provide username (--usern) and (--pw) if
    you like. You'll be prompted if you don't so dunworrybout it mate.
    \n
    Saved user/pass goes to system keyring/credential vault.
    \n
    Messages you prefix with a zero-width space (U+200B) will not be deleted.
    """
    if sum((bool(age_seconds), bool(age_minutes), bool(age_hours))) > 1:
        raise click.UsageError("Use only one --age- option.")

    if age_seconds:
        age = age_seconds
    elif age_minutes:
        age = int(age_minutes * 60)
    elif age_hours:
        age = int(age_hours * 60 * 60)
    else:
        age = int(config.data.get("del_age"))
    echo("Will delete messages older than {} seconds.".format(age))

    if del_hist:
        echo("Will search history for messages to delete.")
        if log_limit:
            echo("Will search {} history messages at a time.".format(log_limit))
    else:
        if log_limit:
            echo("log_limit set to {}.".format(log_limit))

    config.data["last_username"] = usern
    config.data["del_age"] = str(age) # TODO this is iffy
    config.data["log_limit"] = str(log_limit) # TODO this is iffy
    config.data["del_hist"] = str(del_hist).lower() # TODO just as iffy

    stored_pw = keyring.get_password(keyring_service, usern)

    pw_different = stored_pw != pw
    # Does it matter that this can be used to reveal the stored password?
    if stored_pw and pw_different:
        echo("Entered password is different to stored password.")
    elif stored_pw:
        echo("Using stored password for ", nl=False)
        secho(usern, bold=True)
    if (config.has_changed() or pw_different) and saveprompt:
        save_deetz = click.confirm("Save settings?",
                                   default=config.config.getboolean(
                                       appname,
                                       "save_deetz_default_prompt")
                                   )
        save_deetz_default_prompt = str(save_deetz).lower()
        if save_deetz:
            keyring.set_password(keyring_service, usern, pw)
            config.data["save_deetz_default_prompt"] = save_deetz_default_prompt
            config.generate_config()
        else:
            # Save the fact that the user
            # doesn't want to save their settings.
            tempdata = config._old_data.copy()
            tempdata["save_deetz_default_prompt"] = save_deetz_default_prompt
            config.generate_config(tempdata)

    echo("Starting deleter.")
    deleter = DeleterClient(delete_history=del_hist,
                            del_age=age,
                            log_limit=log_limit)
    try:
        deleter.run(usern, pw)
    except LoginFailure:
        secho("Login failed! Try again with different credentials.",
              bold=True, fg="red")
        sys.exit("Login failure")