import os
from configparser import SafeConfigParser

import appdirs


appname = os.path.basename(os.path.dirname(__file__))
config_dir = appdirs.user_config_dir(appname, False)
configfn = "{}.ini".format(appname)
config_path = os.path.join(config_dir, configfn)

# Default values
data = {
    "last_username": "",
    "del_hist": "true",
    "del_age": "172800", # 48h
    "log_limit": "1000",
    "save_deetz_default_prompt": "true"
}

def get_config():
    config = SafeConfigParser()
    if not user_config_exists():
        generate_config()
    config.read([config_path])
    return config


def user_config_exists():
    return os.path.isfile(config_path)


def generate_config(configdata=None):
    global _old_data

    if not configdata:
        configdata = data

    #print("Writing config")
    config = SafeConfigParser(allow_no_value=True)

    config.add_section(appname)
    config.set(appname, "; Last username/email to login.")
    config.set(appname, "last_username", configdata["last_username"])

    config.set(appname, "; Max message age. "
               "How many seconds should messages live for?")
    config.set(appname, "del_age", str(configdata["del_age"]))

    config.set(appname, "; Go through history? or only watch for new messages")
    config.set(appname, "del_hist", configdata["del_hist"])

    config.set(appname, "; Number of history/log messages to retrieve at a time.")
    config.set(appname, "log_limit", configdata["log_limit"])

    config.set(appname, "; Default option (true/false) to save to config or not?")
    config.set(appname, "; Not much point changing this because the last option is always remembered here.")
    config.set(appname, "save_deetz_default_prompt", configdata["save_deetz_default_prompt"])

    # save / write to file
    os.makedirs(config_dir, exist_ok=True)
    with open(config_path, "w") as f:
        config.write(f)

    _old_data = configdata.copy()


config = get_config()

for setting_name in config.options(appname):
    data[setting_name] = config.get(appname, setting_name)

_old_data = data.copy()

def has_changed():
    if _old_data != data:
        return True
    return False


#class Config(object):
    #def __getattr__()