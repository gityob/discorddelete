from asyncio import coroutine, sleep
import os
import json

import discord
from retrying import retry
import arrow
import aiohttp
from click import echo, secho

from .config import config_dir


def is_connection_err(exc):
    if isinstance(exc, TimeoutError):
        return True
    if isinstance(exc, aiohttp.errors.ServerDisconnectedError):
        return True
    return False


class DeleterClient(discord.Client):
    def __init__(self, delete_history, del_age, log_limit=1000,
                 *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.should_delete_history = delete_history
        self.del_age = del_age
        self.log_limit = log_limit
        self.num_queued = 0
        self.chanstyle = {"fg": "blue", "bold": True}
        self.servstyle = {"fg": "green", "bold": True}
        self.datastyle = {"fg": "yellow", "bold": True}
        self.magicpre = "\u200b"


    @retry(retry_on_exception=is_connection_err,
           wait_exponential_multiplier=1000,
           wait_exponential_max=10000)
    def run(self, *args, **kwargs):
        echo("Logging into Discord may take ~20s. Blame ", nl=False)
        secho("them", underline=True, nl=False)
        echo(".")
        super().run(*args, **kwargs)


    @coroutine
    def on_message(self, msg):
        def msg_deleted_callback(msg):
            secho("DELETED: ", nl=False, fg="red")
            self.print_message(msg, True)
            self.num_queued -= 1
            self.print_queued()

        if msg.author.id != self.user.id:
            return

        if msg.content.startswith(self.magicpre):
            return

        self.num_queued += 1
        self.print_queued()
        yield from self.delete_message(msg, self.del_age, msg_deleted_callback)


    def print_queued(self):
        secho("    --- ", nl=False, dim=True)
        secho(str(self.num_queued), nl=False, **self.datastyle)
        echo(" message(s) queued for deletion.", nl=False)
        secho(" ---    ", dim=True)


    @staticmethod
    def print_message(msg, brief=False, briefchars=50):
        if msg.channel.type == discord.ChannelType.text:
            name = "{}/#{}".format(msg.channel.server.name, msg.channel.name)
        else:
            name = str(msg.channel)
        if brief:
            brieftime = arrow.get(msg.timestamp).humanize()
            if len(msg.clean_content) > briefchars:
                briefsuffix = "..."
            else:
                briefsuffix = ""
            briefcontent = msg.clean_content[:briefchars] + briefsuffix
            secho("[{}] {}: ".format(brieftime, msg.author.name), nl=False, dim=True)
            echo(briefcontent)
        elif not brief:
            echo("{}\n{}\n{}:\n  {}".format(msg.timestamp, name, msg.author,
                                            msg.clean_content))


    @coroutine
    def on_ready(self):
        echo("Ready.")
        if self.should_delete_history:
            yield from self.delete_history()


    @coroutine
    def delete_history(self):
        def earlier_msg_dt(chan):
            """
            Look on disk for the result of save_newest_msg_dt()
            and return the matching datetime.
            """
            fn = "newest_seen_messages.json"
            fp = os.path.join(config_dir, fn)
            data = {}
            try:
                with open(fp, "r") as f:
                    data = json.load(f)
            except FileNotFoundError:
                pass
            stamp = data.get(chan.id, {}).get("newest_msg_stamp")
            if stamp:
                # after (Message or datetime)
                # The message or date after which all returned messages must be
                # If a date is provided it must be a timezone-naive datetime
                # representing UTC time.
                # https://discordpy.readthedocs.io/en/latest/api.html#discord.Client.logs_from
                return arrow.get(stamp).to("UTC").naive


        def save_newest_msg_dt(chan, dt):
            """
            Save dt to disk where it can be accessed later by chan id.
            """
            fn = "newest_seen_messages.json"
            fp = os.path.join(config_dir, fn)
            if (chan.type == discord.ChannelType.group or
                chan.type == discord.ChannelType.private):
                name = str(chan)
            else:
                name = "{}/#{}".format(chan.server.name, chan.name)
            newbit = {chan.id: {"name": name,
                                "newest_msg_stamp": dt.isoformat()
                                }
                      }
            old = {}
            try:
                with open(fp, "r") as f:
                    old = json.load(f)
            except FileNotFoundError:
                pass
            new = old.copy()
            new.update(newbit)
            with open(fp, "w") as f:
                json.dump(new, f)
            secho("Wrote newest seen message to file.", dim=True)


        @coroutine
        def process_chan(chan):
            """
            Load channel logs and delete messages.
            Returns a tuple (cnt, newest_msg_dt)
            where cnt is the number of messages deleted
            and newest_msg_dt is a datetime object of the most recent channel
            message that was processed sucessfully.
            """
            lastmsg = msg = None
            cnt = 0
            try:
                mymember = chan.server.get_member(self.user.id)
                when_i_joined = arrow.get(mymember.joined_at)
            except AttributeError:
                # PrivateChannel has no .server
                when_i_joined = arrow.get(0)
            else:
                del mymember # so I'm not tempted/don't accidentally use it later
            newest_msg_dt = None
            docontinue = True

            secho("Processing ", nl=False, bold=True)
            secho(str(chan), nl=False, **self.chanstyle)
            secho(": ", bold=True)

            earlier = earlier_msg_dt(chan)
            if earlier:
                secho("Checking for messages since ", nl=False, dim=True)
                secho(earlier.strftime("%-I:%M%p %b %-d %Y"), dim=True)

            while docontinue: # keep loading logs back to the beginning of time
                i = 0
                try:
                    echo("Loading (up to) {} messages from " \
                         .format(self.log_limit), nl=False)
                    secho(str(chan), nl=False, **self.chanstyle)
                    echo("...")
                    logs = yield from self.logs_from(chan, self.log_limit,
                                                     before=msg,
                                                     after=earlier)
                except (discord.errors.Forbidden,
                        discord.errors.NotFound) as e:
                    secho(str(e), dim=True)
                    logs = None
                if not logs:
                    secho("No logs for this channel.", dim=True)
                    break
                for i, msg in enumerate(logs, start=1):
                    msgtime = arrow.get(msg.timestamp)
                    time_limit = arrow.now().replace(seconds=-self.del_age)
                    if msgtime > time_limit:
                        # message is not yet self.del_age seconds old
                        # leave it for now and catch it next time or something
                        continue

                    if msgtime < when_i_joined:
                        echo("Message pre-date's user joining server.")
                        docontinue = False
                        break

                    diddelete = False
                    if (msg.author.id == self.user.id
                        and not msg.content.startswith(self.magicpre)):
                        assert msg.author.name == self.user.name, "WTF"
                        diddelete = yield from self.delete_message(msg)
                    if diddelete:
                        cnt += 1
                        secho("DELETED: ", nl=False, fg="red")
                        self.print_message(msg, True)
                    else:
                        if not newest_msg_dt:
                            newest_msg_dt = msg.timestamp

                secho("Processed {} messages.".format(i), dim=True)

                # i being less than self.log_limit is a good indication
                # that the log is done - and we'll use that to
                # save some work here - but we don't have to
                if i < self.log_limit:
                    secho("Didn't get all the messages we asked for. "
                          "Must not be any more.", dim=True)
                    break

                if msg:
                    secho("Oldest message in this batch was from {} ({})."
                          .format(msg.timestamp.strftime("%-I:%M%p %b %-d %Y"),
                                  arrow.get(msg.timestamp).humanize()),
                          dim=True)
                else:
                    secho("No messages?", dim=True)
                    break
                if lastmsg and lastmsg.id == msg.id:
                    secho("Got the same message as last time.", dim=True)
                    break
                lastmsg = msg
            if cnt:
                echo("Deleted ", nl=False)
                secho(str(cnt), nl=False, **self.datastyle)
                echo(" message(s) in ", nl=False)
                secho(str(chan), **self.chanstyle)

            return (cnt, newest_msg_dt)
        echo("Searchin' dat backlog.")
        total_deleted = 0
        for serv in self.servers:
            echo("Going through channels in ", nl=False)
            secho(serv.name, **self.servstyle)

            for chan in serv.channels:
                if chan.type == discord.ChannelType.voice:
                    secho(str(chan), nl=False, **self.chanstyle)
                    echo(" is a voice channel. Skipping.")
                    continue
                deleted, newest_msg_dt = yield from process_chan(chan)

                total_deleted += deleted

                # Write timestamp of newest msg we've seen to disk so
                # next run we can pass it to logs_from(after=
                # End result is not having to go to the beginning
                # of any channel more than once.
                # Eventually we just 'check in' and delete any messages
                # that have occured since we last checked.
                if newest_msg_dt:
                    save_newest_msg_dt(chan, newest_msg_dt)

                secho("Done ", bold=True, nl=False)
                echo("with ", nl=False)
                secho(serv.name, nl=False, **self.servstyle)
                echo("/", nl=False)
                secho("#{}".format(chan.name), nl=False, **self.chanstyle)
                echo(".")
        secho("Going through private channels/messages", bold=True)
        for chat in self.private_channels:
            deleted, newest_msg_dt = yield from process_chan(chat)

            total_deleted += deleted

            if newest_msg_dt:
                save_newest_msg_dt(chat, newest_msg_dt)

            secho("Done ", bold=True, nl=False)
            echo("with ", nl=False)
            secho(str(chat), nl=False, **self.chanstyle)
            echo(".")
        echo("Deleted ", nl=False)
        secho(str(total_deleted), nl=False, **self.datastyle)
        echo(" backlog message(s) in total.")
        self.close()


    @coroutine
    def conditional_delete(self, msg, predicate):
        if predicate(msg):
            yield from self.delete_message(msg)
            return True


    @coroutine
    @retry(retry_on_result=lambda x: x is None,
           wait_exponential_multiplier=1000,
           wait_exponential_max=10000,
           stop_max_delay=60000)
    def delete_message(self, msg, delay=0, callback=lambda x: True):
        yield from sleep(delay)
        try:
            yield from super().delete_message(msg)
        except discord.errors.NotFound as e:
            #secho(str(e), dim=True)
            return False
        except discord.errors.Forbidden as e:
            secho("Failed to delete msg! - " + str(e), fg="red")
            return False
        callback(msg)
        return True