from setuptools import setup, find_packages

from discorddelete import __version__
from discorddelete.config import generate_config

print("Generating default config")
generate_config()

setup(
    name="discorddelete",
    version=__version__,
    description="Purge your discord history.",
    packages=find_packages(),
    include_package_data=True,
    install_requires=[
        "discord.py>=0.11.0",
        "appdirs>=1.4.0",
        "click>=6.6",
        "retrying>=1.3.3",
        "keyring>=9.3.1",
        "arrow"
    ],
    entry_points={
        "console_scripts": [
            "discdel = discorddelete.cli:cli"
        ]
    }
)

